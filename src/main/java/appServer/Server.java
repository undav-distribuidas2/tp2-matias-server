package appServer;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class Server {
    public static void main(String[] args) throws IOException, ParserConfigurationException {
        //XMLValidate validator = new XMLValidate();
        //System.out.println(validator.validateWithExtXSDUsingSAX("/home/matias/workspace-intellij/serverSocketXML/display1.xml", "/home/matias/workspace-intellij/serverSocketXML/display1.xsd"));
        SocketServer server = new SocketServer();
        server.runServer();

    }
}
