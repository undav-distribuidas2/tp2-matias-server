package appServer;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServer {

    public final static int SOCKET_PORT = 13267;  // you may change this
    private XMLValidate validator;

    public XMLValidate getValidator() { return validator; }
    public void setValidator(XMLValidate validator) { this.validator = validator; }

    public SocketServer(){
        XMLValidate validator = new XMLValidate();
        setValidator(validator);
    }

    public void runServer() throws IOException{
        FileInputStream fileInputStream = null;
        BufferedInputStream bufferedInputStream = null;
        OutputStream outPutStream = null;
        ServerSocket serverSocket = null;
        Socket socket = null;
        String XMS_FILE = "/home/matias/workspace-intellij/serverSocketXML/display.xsd";
        try {
            serverSocket = new ServerSocket(SOCKET_PORT);
            while (true) {
                System.out.println("Waiting...");
                try {
                    socket = serverSocket.accept();
                    System.out.println("Accepted connection : " + socket);
                    //Recibo el nombre del archivo a enviar
                    ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                    String fileName = (String) objectInputStream.readObject();
                    // send file
                    String FILE_TO_SEND = "/home/matias/workspace-intellij/serverSocketXML/" + fileName +".xml";
                    if(getValidator().validateWithExtXSDUsingSAX(FILE_TO_SEND, XMS_FILE)){
                        System.out.println("validated !");
                        File myFile = new File (FILE_TO_SEND);
                        byte [] mybytearray  = new byte [(int)myFile.length()];
                        fileInputStream = new FileInputStream(myFile);
                        bufferedInputStream = new BufferedInputStream(fileInputStream);
                        bufferedInputStream.read(mybytearray,0,mybytearray.length);
                        outPutStream = socket.getOutputStream();
                        System.out.println("Sending " + FILE_TO_SEND + "(" + mybytearray.length + " bytes)");
                        outPutStream.write(mybytearray,0,mybytearray.length);
                        outPutStream.flush();
                        System.out.println("Done.");
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                } finally {
                    if (bufferedInputStream != null) bufferedInputStream.close();
                    if (outPutStream != null) outPutStream.close();
                    if (socket!=null) socket.close();
                }
            }
        }
        finally {
            if (serverSocket != null) serverSocket.close();
        }

    }

}

